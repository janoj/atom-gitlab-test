'use babel';

import GitlabTestView from '../lib/gitlab-test-view';

describe('GitlabTestView', () => {
  it('has one valid test', () => {
    expect('life').toBe('easy');
  });
});
