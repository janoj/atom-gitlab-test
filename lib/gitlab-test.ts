'use babel';

import GitlabTestView from './gitlab-test-view';
import { CompositeDisposable } from 'atom';


export default {

  gitlabTestView: null,
  modalPanel: null,
  subscriptions: null,

  activate(state) {
    this.gitlabTestView = new GitlabTestView(state.gitlabTestViewState);
    this.modalPanel = atom.workspace.addModalPanel({
      item: this.gitlabTestView.getElement(),
      visible: false
    });

    // Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    this.subscriptions = new CompositeDisposable();

    // Register command that toggles this view
    this.subscriptions.add(atom.commands.add('atom-workspace', {
      'gitlab-test:toggle': () => this.toggle()
    }));
  },

  deactivate() {
    this.modalPanel.destroy();
    this.subscriptions.dispose();
    this.gitlabTestView.destroy();
  },

  serialize() {
    return {
      gitlabTestViewState: this.gitlabTestView.serialize()
    };
  },

  toggle() {
    console.log('GitlabTest was toggled!');
    return (
      this.modalPanel.isVisible() ?
      this.modalPanel.hide() :
      this.modalPanel.show()
    );
  }

};
